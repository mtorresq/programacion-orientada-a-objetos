
import java.io.*;

public class Main {

    static Catalogo[] catalogos = new Catalogo[10];
    static Cliente[] clientes = new Cliente[30];
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static int posicion = 0;

    public static void main(String[] args) throws IOException {
        //	// write your code here
        mostrarMenu();
    }

    static void mostrarMenu() throws IOException{
        int opcion = -1;
        do{
            out.println("1.Registrar catálogos");
            out.println("2.Listar catálogos");
            out.println("3.Registrar clientes");
            out.println("4.Listar clientes");
            out.println("0.Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while(opcion != 0);
    }

    static int seleccionarOpcion() throws IOException{
        out.println("Digite la opción");
        return Integer.parseInt(in.readLine());
    }

    static void procesarOpcion(int opcion) throws IOException{
        switch (opcion){
            case 0:
                out.println("Se ha salido del menú");
                break;
            case 1:
                registrarCatalogos();
                break;
            case 2:
                listarCatalogos();
                break;
            case 3:
                registrarClientes();
                break;
            case 4:
                listarClientes();
                break;
            default:
                out.println("Opción inválida");
        }
    }

    static boolean buscarCatalogo(String nombre, Catalogo [] arregloCatalogos){
        for (int i = 0; i < arregloCatalogos.length; i++){
            if (arregloCatalogos[i] != null && arregloCatalogos[i].nombre.equals(nombre)){
                return true;
            }
        }
        return false;
    }

    static boolean buscarCliente(String correo, Cliente [] arregloClientes){
        for (int i = 0; i < arregloClientes.length; i++){
            if (arregloClientes[i] != null && arregloClientes[i].correo.equals(correo)){
                return true;
            }
        }
        return false;
    }

    static void registrarCatalogos() throws IOException{

        String datos;
        String fecha;
        String nombre;

        out.println("Ingrese la fecha de creación del catálogo");
        fecha = in.readLine();
        out.println("Ingrese el nombre para el catálogo, debe estar compuesto por el nombre del mes y el año de su creación");
        nombre = in.readLine();

        if (buscarCatalogo(nombre, catalogos)){
            out.println("El nombre " + nombre + " ya existía");
            return;
        }

        Catalogo miCatalogo = new Catalogo();
        miCatalogo.fecha = fecha;
        miCatalogo.nombre = nombre;
        catalogos[posicion] = miCatalogo;
        posicion++;;


    }

    static void listarCatalogos(){
        out.println("Lista de catálogos registrados");
        for (int i = 0 ; i < posicion ; i++){
            out.println(catalogos[i].informacion());
        }
    }

    static void registrarClientes() throws IOException{

        String datos;
        String nombre;
        String primerApellido;
        String segundoApellido;
        String direccion;
        String correo;

        out.println("Ingrese el nombre del cliente");
        nombre = in.readLine();
        out.println("Ingrese el primer apellido");
        primerApellido = in.readLine();
        out.println("Ingrese el segundo apellido");
        segundoApellido = in.readLine();
        out.println("Ingrese la dirección");
        direccion = in.readLine();
        out.println("Ingrese el correo electrónico");
        correo = in.readLine();

        if (buscarCliente(correo, clientes)){
            out.println("Ya existe un cliente registrado con el correo " + correo);
            return;
        }

        Cliente miCliente = new Cliente();
        miCliente.nombre = nombre;
        miCliente.primerApellido = primerApellido;
        miCliente.segundoApellido = segundoApellido;
        miCliente.direccion = direccion;
        miCliente.correo = correo;
        clientes[posicion] = miCliente;
        posicion++;
    }

    static void listarClientes(){
        out.println("Lista de clientes registrados");
        for (int i = 0 ; i < posicion ; i++){
            out.println(clientes[i].informacion());
        }
    }

}
